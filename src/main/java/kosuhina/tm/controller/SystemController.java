package kosuhina.tm.controller;

public class SystemController {
    /**
     * Данные о версии программы
     */
        public int displayVersion() {
        System.out.println("1.0.0");
        return 0;
    }

    /**
     * Данные о разработчике
     */
        public int displayAbout() {
        System.out.println("Svetlana Kosuhina");
        System.out.println("lana__svet@list.ru");
        return 0;
    }

    /**
     *Выход из программы
     */
        public int displayExit() {
        System.out.println("Terminate program...");
        return 0;
    }

    /**
     *Сообщение об ошибке
     */
        public int displayError() {
        System.out.println("Error! Unknown program argument...");
        return -1;
    }

}
